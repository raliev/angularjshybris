export class Product {
  code: string;
  name: string;
  availableForPickup : string;
  description : string;
  manufacturer : string;
  price : string;
  stockLevel : string;
  summary : string;
  url : string;
  imageUrl : string;
}
